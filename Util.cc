#include "Util.hh"

bool equal(const Point& p1, const Point& p2, double epsilon = 1e-6) {
    return std::abs(p1[0] - p2[0]) < epsilon &&
           std::abs(p1[1] - p2[1]) < epsilon &&
           std::abs(p1[2] - p2[2]) < epsilon;
}


double euclideanDistance(const Point& p1, const Point& p2) {
    double dx = p2[0] - p1[0];
    double dy = p2[1] - p1[1];
    double dz = p2[2] - p1[2];

    return std::sqrt(dx * dx + dy * dy + dz * dz);
}

double curvilinearDistance(const Point& point1, const Point& point2, const PolyLine& polygon, double max){
    // Retrieve the points of the polygon
    const std::vector<Point>& points = polygon.points();

    // Find the point in the polygon that matches point1
    auto it = std::find(points.begin(), points.end(), point1);
    if (it == points.end()) {
        // Point1 is not in the polygon, return max
        return max;
    }

    double forwardDistance = 0;
    double backwardDistance = 0;

    // Calculate distance in both forward and backward directions
    for (auto it_forward = it + 1, it_backward = it - 1; ; ++it_forward, --it_backward) {
        // Check if we've wrapped around the polygon in either direction
        if (it_forward == points.end()) it_forward = points.begin();
        if (it_backward == points.begin()) it_backward = points.end() - 1;

        // Add the distance between the current point and the previous point in the polygon
        forwardDistance += euclideanDistance(*it_forward, *(it_forward - 1));
        backwardDistance += euclideanDistance(*it_backward, *(it_backward + 1));

        // Check if we've exceeded max distance in both direction
        if (forwardDistance > max && backwardDistance > max) {
            return max;
        }

        // Check if we've found point2 in the forward direction
        if (equal(*it_forward, point2)) {
            return forwardDistance;
        }

        // Check if we've found point2 in the backward direction
        if (equal(*it_backward, point2)) {
            return backwardDistance;
        }
    }
}


std::pair<ACG::Vec2d, ACG::Vec2d> getMinMaxCoords(const PolyLine& polyline)
{
    // Initialize minCoords and maxCoords with the first point of the polyline
    ACG::Vec2d minCoords = {polyline.points()[0][0], polyline.points()[0][1]};
    ACG::Vec2d maxCoords = {polyline.points()[0][0], polyline.points()[0][1]};

    // Get points of the polyline
    std::vector<Point> points = polyline.points();

    // Iterate over each point in points
    for (const Point& point : points)
    {
        minCoords[0] = std::min(minCoords[0], point[0]);
        minCoords[1] = std::min(minCoords[1], point[1]);
        maxCoords[0] = std::max(maxCoords[0], point[0]);
        maxCoords[1] = std::max(maxCoords[1], point[1]);
    }

    return std::make_pair(minCoords, maxCoords);
}

std::pair<ACG::Vec2d, ACG::Vec2d> getMinMaxCoords(const PolyLines& polylines)
{
    // Initialize minCoords and maxCoords with the first point of the first polyline
    ACG::Vec2d minCoords = {polylines[0].points()[0][0], polylines[0].points()[0][1]};
    ACG::Vec2d maxCoords = {polylines[0].points()[0][0], polylines[0].points()[0][1]};

    // Iterate over each polyline in polylines
    for (const PolyLine& polyline : polylines)
    {
        // Get points of the polyline
        std::vector<Point> points = polyline.points();

        // Iterate over each point in points
        for (const Point& point : points)
        {
            minCoords[0] = std::min(minCoords[0], point[0]);
            minCoords[1] = std::min(minCoords[1], point[1]);
            maxCoords[0] = std::max(maxCoords[0], point[0]);
            maxCoords[1] = std::max(maxCoords[1], point[1]);
        }
    }

    return std::make_pair(minCoords, maxCoords);
}

std::pair<ACG::Vec2d, ACG::Vec2d> getMinMaxCoords(const std::vector<PolyLines>& perimeters)
{
    // Initialize minCoords and maxCoords with the first point of the first polyline of the first perimeter
    ACG::Vec2d minCoords = {perimeters[0][0].points()[0][0], perimeters[0][0].points()[0][1]};
    ACG::Vec2d maxCoords = {perimeters[0][0].points()[0][0], perimeters[0][0].points()[0][1]};

    // Iterate over each perimeter in perimeters
    for (const PolyLines& perimeter : perimeters)
    {
        // Iterate over each polyline in perimeter
        for (const PolyLine& polyline : perimeter)
        {
            // Get points of the polyline
            std::vector<Point> points = polyline.points();

            // Iterate over each point in points
            for (const Point& point : points)
            {
                minCoords[0] = std::min(minCoords[0], point[0]);
                minCoords[1] = std::min(minCoords[1], point[1]);
                maxCoords[0] = std::max(maxCoords[0], point[0]);
                maxCoords[1] = std::max(maxCoords[1], point[1]);
            }
        }
    }

    return std::make_pair(minCoords, maxCoords);
}

void saveGridToPNG(const std::vector<std::vector<int>>& grid, const std::string& filename) {
    size_t gridWidth = grid.size();
    size_t gridHeight = grid[0].size();

    std::vector<unsigned char> image;
    image.resize(gridWidth * gridHeight * 4);
    for (size_t x = 0; x < gridWidth; ++x) {
        for (size_t y = 0; y < gridHeight; ++y) {
            image[4 * gridWidth * y + 4 * x + 0] = 255 * grid[x][y];
            image[4 * gridWidth * y + 4 * x + 1] = 255 * grid[x][y];
            image[4 * gridWidth * y + 4 * x + 2] = 255 * grid[x][y];
            image[4 * gridWidth * y + 4 * x + 3] = 255;
        }
    }
    unsigned error = lodepng::encode(filename, image, gridWidth, gridHeight);
    if (error) std::cout << "encoder error " << error << ": " << lodepng_error_text(error) << std::endl;
}
