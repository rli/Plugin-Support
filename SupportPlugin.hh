#ifndef SUPPORTPLUGIN_HH_INCLUDED
#define SUPPORTPLUGIN_HH_INCLUDED

#include <QComboBox>

#include <OpenFlipper/BasePlugin/BaseInterface.hh>
#include <OpenFlipper/common/Types.hh>
#include <OpenFlipper/BasePlugin/ToolboxInterface.hh>
#include <OpenFlipper/BasePlugin/LoggingInterface.hh>

#include "Util.hh"
#include "Image.hh"


class SupportPlugin : public QObject, BaseInterface, ToolboxInterface, LoggingInterface
{
Q_OBJECT
Q_INTERFACES(BaseInterface)
Q_INTERFACES(ToolboxInterface)
Q_INTERFACES(LoggingInterface)
Q_PLUGIN_METADATA(IID "org.OpenFlipper.Plugins.Plugin-Support")

signals:
  // BaseInterface
  void updateView();
  void updatedObject(int _id, const UpdateType& _type);

  // LoggingInterface
  void log(Logtype _type, QString _message);
  void log(QString _message);

  // ToolboxInterface
  void addToolbox( QString _name  , QWidget* _widget);

public :

  SupportPlugin() {};  
  ~SupportPlugin() {};
  
  QString name() { return QString("SupportPlugin"); };
  QString description() { return QString("generates 3D print support structures"); };

private:
  QComboBox* supportTypeComboBox_;

  
  struct SupportPoint 
  {
    Point point;
    int layer;
  };

  bool isUnsupported(const Point& point, size_t layer, const std::vector<PolyLines>& perimeters);
  double cancelingDistance = 2;

  std::vector<Point> selectSupportPoints(const Slices& slices);

private slots:

  //BaseInterface
  void initializePlugin();
  void pluginsInitialized();

public slots:
  QString version() { return QString("1.0"); };
};
#endif
