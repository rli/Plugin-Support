#include "SupportPlugin.hh"
#include "OpenFlipper/BasePlugin/PluginFunctions.hh"

#include <QVBoxLayout>
#include <vector>

void SupportPlugin::initializePlugin()
{
    QWidget* widget = new QWidget();
    supportTypeComboBox_ = new QComboBox();
    supportTypeComboBox_->addItem("Scaffold");

    QVBoxLayout* layout = new QVBoxLayout(widget);
    layout->addWidget(supportTypeComboBox_);
    
    emit addToolbox(tr("Support"), widget);

}

std::vector<Point> SupportPlugin::selectSupportPoints(const Slices& slices) {

    std::vector<Point> supportPoints;
    std::vector<PolyLines> perimeters;
    for (const auto& slice : slices) {
        perimeters.push_back(slice.outerWalls);
    }

    int layer = 0;
    for (const auto& perimeter : perimeters) {
        for (const auto& polygon : perimeter){
            for (const auto& u : polygon.points()) {
                if (isUnsupported(u, layer, perimeters)) {
                    bool exists = std::any_of(supportPoints.begin(), supportPoints.end(),
                                            [&u, &polygon, this](const Point& v) {
                                                 return curvilinearDistance(u, v, polygon, cancelingDistance) < cancelingDistance;
                                            });

                    if (!exists) {
                        supportPoints.push_back(u);
                    }
                }
            }
        }
        layer++;
    }
    return supportPoints;
}

bool SupportPlugin::isUnsupported(const Point& point, size_t layer, const std::vector<PolyLines>& perimeters) {
    // Check if layer is valid
    if (layer == 0 || layer > perimeters.size()) {
        return false;
    }

    // Get the layer just below
    PolyLines perimeter = perimeters[layer - 1];
    ACG::Vec2d minCoords = getMinMaxCoords(perimeters).first;
    ACG::Vec2d maxCoords = getMinMaxCoords(perimeters).second;
    Image layerImage(perimeter, minCoords, maxCoords);
    

    double nozzleDiameter = 0.4;
    double pixelResolution = 0.05;
    int diskRadius = nozzleDiameter / (2 * pixelResolution);  // calculate disk radius in pixels

    int unsupportedPixels = 0;
    int totalPixels = 0;

    // Iterate through each pixel within the disk
    for (int i = -diskRadius; i <= diskRadius; ++i) {
        for (int j = -diskRadius; j <= diskRadius; ++j) {
            if (i * i + j * j <= diskRadius * diskRadius) {  // Check if pixel is inside disk
                Point pixelPoint = point + Point(i * pixelResolution, j * pixelResolution, 0);
                if (!layerImage.isPointInObject(pixelPoint)) {
                    unsupportedPixels++;
                }
                totalPixels++;
            }
        }
    }

    // Check if more than 50% of the disk is unsupported
    return static_cast<double>(unsupportedPixels) / totalPixels > 0.5;
}
