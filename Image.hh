#ifndef IMAGE_HH_INCLUDED
#define IMAGE_HH_INCLUDED

#include "Util.hh"

class Image {
private:
    ACG::Vec2d minCoords;
    ACG::Vec2d maxCoords;
    double pixelWidth;
    std::vector<std::vector<int>> grid;

public:
    Image(const PolyLine& polyLine, ACG::Vec2d minCoords, ACG::Vec2d maxCoords, double pixelWidth = 0.05);
    Image(const PolyLines& polyLines, ACG::Vec2d minCoords, ACG::Vec2d maxCoords, double pixelWidth = 0.05);
    void drawLineSegment(const ACG::Vec3d& point1, const ACG::Vec3d& point2, double lineWidth = 0.4);
    bool isPointInObject(const ACG::Vec3d& point);
};

#endif // IMAGE_HH_INCLUDED
