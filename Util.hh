#include "Plugin-SS23Slicer/Slicer-Interface.hh"
#include "OpenFlipper/libs_required/ACG/Math/VectorT.hh"

#include <cmath>
#include <vector>

#include "lodepng/lodepng.h"

typedef std::vector<Slice> Slices;
typedef ACG::Vec3d Point;
typedef ACG::PolyLineT<Point> PolyLine;
typedef std::vector<PolyLine> PolyLines;

bool equal(const Point& p1, const Point& p2, double epsilon);
double euclideanDistance(const Point& p1, const Point& p2);
double curvilinearDistance(const Point& point1, const Point& point2, const PolyLine& polygon, double max);

std::pair<ACG::Vec2d, ACG::Vec2d> getMinMaxCoords(const PolyLine& polyline);
std::pair<ACG::Vec2d, ACG::Vec2d> getMinMaxCoords(const PolyLines& polylines);
std::pair<ACG::Vec2d, ACG::Vec2d> getMinMaxCoords(const std::vector<PolyLines>& perimeters);

void saveGridToPNG(const std::vector<std::vector<int>>& grid, const std::string& filename);
