#include "Image.hh"

// Define these helper functions or similar to convert between coordinates and pixels
int coordToPixel(double coord, double minCoord, double pixelWidth) {
    return static_cast<int>((coord - minCoord) / pixelWidth);
}

//ACG::Vec3d pixelToCoord(int pixel, double minCoord, double pixelWidth) {
//    return minCoord + pixel * pixelWidth;
//}

void gridInit(std::vector<std::vector<int>>& grid, int width, int height) {
    // Initialize grid to be a 2D vector of size width x height
    grid.resize(width);
    for (size_t i = 0; i < grid.size(); ++i) {
        grid[i].resize(height);
    }
    
    // // Initialize all elements of grid to 0
    // for (size_t x = 0; x < grid.size(); ++x) {
    //     for (size_t y = 0; y < grid[x].size(); ++y) {
    //         grid[x][y] = 0;
    //     }
    // }
}

Image::Image(const PolyLine& polyLine, ACG::Vec2d minCoords, ACG::Vec2d maxCoords, double pixelWidth)
    : minCoords(minCoords), maxCoords(maxCoords), pixelWidth(pixelWidth){
    // Initialize grid based on polyLine and pixelWidth
    size_t gridWidth = coordToPixel(maxCoords[0], minCoords[0], pixelWidth) + 1;
    size_t gridHeight = coordToPixel(maxCoords[1], minCoords[1], pixelWidth) + 1;
    gridInit(grid, gridWidth, gridHeight);

    // Then, draw each line segment in polyLine onto the grid
    auto points = polyLine.points();
    for (size_t i = 0; i < points.size() - 1; ++i) {
        drawLineSegment(points[i], points[i + 1]);

    }

    // Draw the last line segment from the last point to the first point
    drawLineSegment(points[points.size() - 1], points[0]);

    // export grid to png and output
    saveGridToPNG(grid, "grid.png");
}

Image::Image(const std::vector<PolyLine>& polyLines, ACG::Vec2d minCoords, ACG::Vec2d maxCoords, double pixelWidth)
    : minCoords(minCoords), maxCoords(maxCoords), pixelWidth(pixelWidth){
    // Initialize grid based on polyLines and pixelWidth
    size_t gridWidth = coordToPixel(maxCoords[0], minCoords[0], pixelWidth) + 1;
    size_t gridHeight = coordToPixel(maxCoords[1], minCoords[1], pixelWidth) + 1;
    gridInit(grid, gridWidth, gridHeight);

    // Then, draw each line segment in each PolyLine onto the grid
    for (const PolyLine& polyLine : polyLines) {
        auto points = polyLine.points();
        for (size_t i = 0; i < points.size() - 1; ++i) {
            drawLineSegment(points[i], points[i + 1]);
        }

        // Draw the last line segment from the last point to the first point
        drawLineSegment(points[points.size() - 1], points[0]);
    }

    // export grid to png and output
    saveGridToPNG(grid, "grid.png");

}

void Image::drawLineSegment(const ACG::Vec3d& point1, const ACG::Vec3d& point2, double lineWidth) {
    int halfLineWidth = static_cast<int>(lineWidth / 2.0 / pixelWidth);

    // Convert points to pixel coordinates
    int x0 = coordToPixel(point1[0], minCoords[0], pixelWidth);
    int y0 = coordToPixel(point1[1], minCoords[1], pixelWidth);
    int x1 = coordToPixel(point2[0], minCoords[0], pixelWidth);
    int y1 = coordToPixel(point2[1], minCoords[1], pixelWidth);

    // Bresenham's line algorithm
    int dx = abs(x1 - x0), sx = x0 < x1 ? 1 : -1;
    int dy = -abs(y1 - y0), sy = y0 < y1 ? 1 : -1;
    int err = dx + dy, e2;

    while (true) {
        // Instead of setting a single pixel, set a square of pixels around the current pixel
        for (int offsetX = -halfLineWidth; offsetX <= halfLineWidth; offsetX++) {
            for (int offsetY = -halfLineWidth; offsetY <= halfLineWidth; offsetY++) {
                size_t x = x0 + offsetX;
                size_t y = y0 + offsetY;

                // Make sure the coordinates are within the grid
                if (x < grid.size() && y < grid[0].size()) {
                    grid[x][y] = 1;
                }
            }
        }

        if (x0 == x1 && y0 == y1) break;

        e2 = 2 * err;
        if (e2 >= dy) { err += dy; x0 += sx; } // e_xy+e_x > 0
        if (e2 <= dx) { err += dx; y0 += sy; } // e_xy+e_y < 0
    }
}


bool Image::isPointInObject(const ACG::Vec3d& point) {
    // Convert point to pixel coordinates
    int x = coordToPixel(point[0], minCoords[0], pixelWidth);
    int y = coordToPixel(point[1], minCoords[1], pixelWidth);

    // Then, return whether the corresponding element in grid is 1 (inside an object)
    return grid[x][y] == 1;
}
